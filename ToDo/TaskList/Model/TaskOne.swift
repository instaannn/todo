//
//  Task.swift
//  ToDo
//
//  Created by Anna Sycheva on 04.02.2020.
//  Copyright © 2020 AnnaS. All rights reserved.
//

import Foundation

class TaskOne {

    init(name: String) {
        self.name = name
    }
    
    init(name: String, category: String) {
        self.name = name
        self.category = category
    }
    
    init(name: String, category: String, deadline: Date) {
        self.name = name
        self.category = category
        self.deadline = deadline
    }
    
    init(name: String, category: String, priority: String) {
        self.name = name
        self.category = category
        self.priority = priority
    }
    
    init(name: String, category: String, deadline: Date, priority: String) {
        self.name = name
        self.category = category
        self.deadline = deadline
        self.priority = priority
    }
    
    var name: String
    var category: String?
    var priority: String?
    var deadline: Date?
    
}
