//
//  TaskListCell.swift
//  ToDo
//
//  Created by Anna Sycheva on 04.02.2020.
//  Copyright © 2020 AnnaS. All rights reserved.
//

import UIKit

class TaskListCell: UITableViewCell {

    // MARK: Outlets
    
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var taskCategoryLabel: UILabel!
    @IBOutlet weak var taskDeadlineLabel: UILabel!
    
    
    func setTask(task: TaskOne) {
        taskNameLabel.text = task.name
        taskCategoryLabel.text = task.category
    }
    
}
