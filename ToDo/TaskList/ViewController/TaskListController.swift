//
//  TaskListController.swift
//  ToDo
//
//  Created by Anna Sycheva on 03.02.2020.
//  Copyright © 2020 AnnaS. All rights reserved.
//

import UIKit
import CoreData

final class TaskListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Outlets
    
    @IBOutlet private var tableView: UITableView!
    
    // MARK: Private properties
    
    private var taskList: [TaskOne] = [TaskOne(name: "Задача 1"),
                                   TaskOne(name: "Задача 2", category: "Категория 2", priority: "Важная"),
                                   TaskOne(name: "Задача 3", category: "Категория 3", deadline: Date()),
                                   TaskOne(name: "Задача 4", category: "Категория 4")]
    private let dateFormatte = DateFormatter()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nibName = UINib(nibName: "TaskListCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "taskListCell")
        settingDate()
    }
    
    // MARK: Private methods
    
    private func settingDate() {
        dateFormatte.dateStyle = .medium
     //   dateFormatte.dateFormat = "MM.dd.yyyy"
        dateFormatte.locale = Locale(identifier: "ru_Ru")
    }
    
    // MARK: UITableViewDataSource implementation
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "taskListCell", for: indexPath) as? TaskListCell else {
            fatalError("cell type")
        }
        
        let task = taskList[indexPath.row]
        cell.setTask(task: task)

        if let deadline = task.deadline {
            cell.taskDeadlineLabel.text = dateFormatte.string(from: deadline)
        } else {
            cell.taskDeadlineLabel.text = ""
        }
        
        return cell
    }
    

}


